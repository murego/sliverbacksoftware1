
import Vue from 'vue'
import vuetify from './plugins/vuetify' // path to vuetify export
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App'
import router from './router'
import VueCharts from 'vue-chartjs'
import vueResource from 'vue-resource'

import store from './store'
import Axios from 'axios'

window.eventBus=new Vue()

Vue.prototype.$http = Axios;

const token = localStorage.getItem('user-token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(vueResource)
Vue.use(VueCharts);

Vue.config.productionTip=false;
/* eslint-disable no-new */

new Vue({
  vuetify,
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
}).$mount('#app')
