import Vue from 'vue'
import Vuex from 'vuex'
// import axios from './axios-auth';
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'https://sliverback.hotelsconnect.rw/public/api'

export default new Vuex.Store({
  state: {
		// token: localStorage.getItem('token') || null,
        // level: localStorage.getItem('level') || null,
        data:[],
  },
  getters: {
	level(state) {
		return state.level
	  },
	  loggedIn(state) {
		return state.token !== null
	  },
},
  mutations: {
    login(state, level) {
		state.level = level
	  },
	  login(state, token) {
		state.token = token
	  },
  },
  actions: {
	//   for admin panel
    retrieveTodos(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
  
        axios.post('/selectType')
          .then(response => {
            console.log(response)
            context.commit('retrieveTodos', response.data)
          })
          .catch(error => {
            console.log(error)
          })
      },
	// AddPartner (context, authData){
    //     axios.post('/partner',{
	// 		f_name:authData.f_name,
	// 		l_name:authData.l_name,
	// 		email:authData.email,
	// 		username:authData.username,
	// 		phone:authData.phone, 
	// 		identity:authData.identity,
	// 		password:authData.login, 
	// 		confirmPassword:authData.confirmPassword,
	// 		num_allowed:authData.num_allowed,
	// 		level:authData.level,
    //     })
    //     .then(res => {
	// 		console.log(res)
	// 	const data = res.data
	// 	 console.log(data)
	// 						})
    //     .catch(error => console.log(error))
	// },
	// AdminViewAllHotels (context, authData){
    //     axios.post('/partner',{
	// 		f_name:authData.f_name,
	// 		l_name:authData.l_name,
	// 		email:authData.email,
	// 		username:authData.username,
	// 		phone:authData.phone, 
	// 		identity:authData.identity,
	// 		password:authData.login, 
	// 		confirmPassword:authData.confirmPassword,
	// 		num_allowed:authData.num_allowed,
	// 		level:authData.level,
    //     })
    //     .then(res => {
	// 		console.log(res)
	// 	const data = res.data
	// 	 console.log(data)
	// 						})
    //     .catch(error => console.log(error))
	// }
	// end of   for admin panel
  }

})

