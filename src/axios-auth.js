import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://sliverback.hotelsconnect.rw/public/api'
})

// instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance