import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'

// Learn vue js
import AAAAlearn from '@/components/AAAAlearn'
// Learn vue js

// admin profile
import Admin from '@/components/Admin'
import Admin_profile from '@/components/Admin_profile'
import Admin_password from '@/components/Admin_password'
// end of admin profile

// hotel owner profile
import Hotelowner from '@/components/Hotelowner'
import Hotelowner_profile from '@/components/Hotelowner_profile'
import Hotelowner_password from '@/components/Hotelowner_password'
import Branchmanager_supplier_add from '@/components/Branchmanager_supplier_add'
// end of hotel owner profile

import Branchmanager from '@/components/Branchmanager'
import Branchmanager_profile from '@/components/Branchmanager_profile'
import Checkinform from '@/components/Checkinform'
import Checkinreceipt from '@/components/Checkinreceipt'
import Checkoutform from '@/components/Checkoutform'
import Partner from '@/components/Partner'
import Addhotel from '@/components/Addhotel'
import Generalmanager from '@/components/Generalmanager'
import User_add from '@/components/User_add'
import Room_add from '@/components/Room_add'
import Book_add from '@/components/Book_add'
import Vuetifyheader from '@/components/Vuetifyheader'
import Expense_add from '@/components/Expense_add'
import View_partner from '@/components/View_partner'
import View_hotel from '@/components/View_hotel'
import View_manager from '@/components/View_manager'
import View_user from '@/components/View_user'
import View_room from '@/components/View_room'
import View_booking from '@/components/View_booking'
import View_expense from '@/components/View_expense'
import Branchmanager_password from '@/components/Branchmanager_password'


import Front_office from '@/components/Front_office'
import Supervisor from '@/components/Supervisor'
import Housekeeper from '@/components/Housekeeper'
import Roomattendants from '@/components/Roomattendants'
import FB_Supervisor from '@/components/FB_Supervisor'
import Cashiers from '@/components/Cashiers'
import Waiter from '@/components/Waiter'

import Viewexpenses from '@/components/Viewexpenses'
import General_manager from '@/components/General_manager'
import Sales from '@/components/Sales'
import Room_detail from '@/components/Room_detail'

// front-Front_office
import Front_office_booking from '@/components/Front_office_booking'
import Front_office_view_expenses from '@/components/Front_office_view_expenses'
import Front_office_add_expenses from '@/components/Front_office_add_expenses'
import Front_office_view_booking from '@/components/Front_office_view_booking'
import Front_office_profile from '@/components/Front_office_profile'
import Front_office_password from '@/components/Front_office_password'
// end of front-office

// Supervisor
import Supervisor_view_booking from '@/components/Supervisor_view_booking'
import Supervisor_add_booking from '@/components/Supervisor_add_booking'
import Supervisor_profile from '@/components/Supervisor_profile'
import Supervisor_password from '@/components/Supervisor_password'
import Supervisor_view_expense from '@/components/Supervisor_view_expense'
import Supervisor_add_expense from '@/components/Supervisor_add_expense'
// end of Supervisor

// font Front_desk
import Front_Desk from '@/components/Front_Desk'
import Front_desk_view_booking from '@/components/Front_desk_view_booking'
import Front_desk_add_booking from '@/components/Front_desk_add_booking'
import Front_Desk_profile from '@/components/Front_Desk_profile'
import Front_desk_password from '@/components/Front_desk_password'
import Front_Desk_expense_add from '@/components/Front_Desk_expense_add'
import Front_Desk_expense_view from '@/components/Front_Desk_expense_view'
// end of Front_desk

// Room division manager
import Room_division_manager from '@/components/Room_division_manager'
import Room_division_manager_add_room from '@/components/Room_division_manager_add_room'
import Room_division_manager_view_room from '@/components/Room_division_manager_view_room'
import Room_division_manager_view_expenses from '@/components/Room_division_manager_view_expenses'
import Room_division_manager_add_expenses from '@/components/Room_division_manager_add_expenses'
import Room_division_manager_profile from '@/components/Room_division_manager_profile'
import Room_division_manager_password from '@/components/Room_division_manager_password'
// end of Room division manager

// F&B Manager
import FB_Manager from '@/components/FB_Manager'
import FB_manager_create_order from '@/components/FB_manager_create_order'
import FB_manager_view_menu from '@/components/FB_manager_view_menu'
import FB_manager_create_menu from '@/components/FB_manager_create_menu'
import FB_manager_view_order from '@/components/FB_manager_view_order'
import FB_Manager_profile from '@/components/FB_Manager_profile'
import FB_manager_password from '@/components/FB_manager_password'
// end of F&B Manager

// F&B Supervisor
import FB_supervisor_view_menu from '@/components/FB_supervisor_view_menu'
import FB_supervisor_create_menu from '@/components/FB_supervisor_create_menu'
import FB_supervisor_view_order from '@/components/FB_supervisor_view_order'
import FB_supervisor_create_order from '@/components/FB_supervisor_create_order'
import FB_supervisor_profile from '@/components/FB_supervisor_profile'
import FB_supervisor_password from '@/components/FB_supervisor_password'
// end of F&B Supervisor

// Cashiers
import Cashier_view_menu from '@/components/Cashier_view_menu'
import Cashier_create_menu from '@/components/Cashier_create_menu'
import Cashier_view_order from '@/components/Cashier_view_order'
import Cashier_create_order from '@/components/Cashier_create_order'
import Cashiers_profile from '@/components/Cashiers_profile'
import Cashier_password from '@/components/Cashier_password'
// Cashiers

// Waiter or waitress 
import Waiter_create_menu from '@/components/Waiter_create_menu'
import Waiter_view_menu from '@/components/Waiter_view_menu'
import Waiter_view_order from '@/components/Waiter_view_order'
import Waiter_create_order from '@/components/Waiter_create_order'
import Waiter_profile from '@/components/Waiter_profile'
import Waiter_password from '@/components/Waiter_password'
// end of Waiter or waitress 

import Zchart from '@/components/Zchart'
import chartjs from '@/components/chartjs'

// learn vuex course
import AAAAATodos from '@/components/AAAAATodos'
// end of learn vuex course
Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [

    {
      path: '/AAAAATodos', name: 'AAAAATodos', component:AAAAATodos
    },
    {
      path: '/Admin_profile', name: 'Admin_profile', component: Admin_profile
    },
    {
      path: '/Admin_password', name: 'Admin_password', component: Admin_password
    },
    {
      path: '/AAAAlearn', name: 'AAAAlearn', component: AAAAlearn
    },
    {
      path: '/Zchart', name: 'Zchart', component: Zchart
    },
    {
      path: '/chartjs', name: 'chartjs', component: chartjs
    },
    {
      path: '/', name: 'Home', component: Home
    },
    {
      path: '/Login', name: 'Login', component: Login
    },
    {
      path: '/Admin', name: 'Admin', component: Admin
    },

    // Hotelowner
    {
      path: '/Hotelowner', name: 'Hotelowner', component: Hotelowner  
    },
    {
      path: '/Hotelowner_profile', name: 'Hotelowner_profile', component: Hotelowner_profile  
    },
    {
      path: '/Hotelowner_password', name: 'Hotelowner_password', component: Hotelowner_password  
    },

    // end of hotel owner
    {
      path: '/Branchmanager', name: 'Branchmanager', component: Branchmanager
    },
    {
      path: '/Branchmanager_supplier_add', name: 'Branchmanager_supplier_add', component: Branchmanager_supplier_add
    },
    {
      path: '/Branchmanager_profile', name: 'Branchmanager_profile', component: Branchmanager_profile
    },
    {
      path: '/Branchmanager_password', name: 'Branchmanager_password', component: Branchmanager_password
    },
    {
      path: '/Checkinform', name: 'Checkinform', component: Checkinform
    },
    {
      path: '/Checkinreceipt', name: 'Checkinreceipt', component: Checkinreceipt
    },
    {
      path: '/Checkoutform', name: 'Checkoutform', component: Checkoutform
    },
    {
      path: '/Partner', name: 'Partner', component: Partner
    },
    {
      path: '/Addhotel', name: 'Addhotel', component: Addhotel
    },
    {
      path: '/Generalmanager', name: 'Generalmanager', component: Generalmanager
    },
    {
      path: '/User_add', name: 'User_add', component: User_add
    },
    {
      path: '/Room_add', name: 'Room_add', component: Room_add
    },
    {
      path: '/Book_add', name: 'Book_add', component: Book_add
    },
    {
      path: '/Vuetifyheader', name: 'Vuetifyheader', component: Vuetifyheader
    },
    {
      path: '/Expense_add', name: 'Expense_add', component: Expense_add
    },
    {
      path: '/View_partner', name: 'View_partner', component: View_partner
    },
    {
      path: '/View_hotel', name: 'View_hotel', component: View_hotel
    },
    {
      path: '/View_manager', name: 'View_manager', component: View_manager
    },
    {
      path: '/View_user', name: 'View_user', component: View_user
    },
    {
      path: '/View_room/:id', name: 'View_room', component: View_room
    },
    {
      path: '/View_booking', name: 'View_booking', component: View_booking
    },
    {
      path: '/View_expense', name: 'View_expense', component: View_expense
    },
    {
      path: '/Room_division_manager',  name: 'Room_division_manager', component: Room_division_manager
    },
    {
      path: '/Room_division_manager_password',  name: 'Room_division_manager_password', component: Room_division_manager_password
    },
    
    {
      path: '/Front_office', name: 'Front_office',  component:Front_office
    },
    {
      path: '/Supervisor', name: 'Supervisor', component:Supervisor
    },
    {
      path: '/Supervisor_view_expense', name: 'Supervisor_view_expense', component:Supervisor_view_expense
    },
    {
      path: '/Supervisor_add_expense', name: 'Supervisor_add_expense', component:Supervisor_add_expense
    },
    {
      path: '/Supervisor_password', name: 'Supervisor_password', component:Supervisor_password
    },
    {
      path: '/Housekeeper', name: 'Housekeeper', component:Housekeeper
    },
    {
      path: '/Roomattendants', name: 'Roomattendants', component:Roomattendants
    },
    {
      path: '/FB_Manager',  name: 'FB_Manager',  component:FB_Manager
    },
    {
      path: '/FB_manager_password',  name: 'FB_manager_password',  component:FB_manager_password
    },
    {
      path: '/FB_Supervisor', name: 'FB_Supervisor', component:FB_Supervisor
    },
    {
      path: '/Cashiers', name: 'Cashiers', component:Cashiers
    },
    {
      path: '/Waiter', name: 'Waiter',  component:Waiter
    },
    {
      path: '/Waiter_password', name: 'Waiter_password',  component:Waiter_password
    },
    {
      path: '/Viewexpenses',  name: 'Viewexpenses',  component:Viewexpenses
    },
    {
      path: '/General_manager',  name: 'General_manager',  component:General_manager
    },
    {
      path: '/Sales', name: 'Sales', component:Sales
    },
    {
      path: '/Room_detail', name: 'Room_detail', component:Room_detail
    },

    // font-Front_office
    {
      path: '/Front_office_booking', name: 'Front_office_booking', component:Front_office_booking
    },
    {
      path: '/Front_office_profile', name: 'Front_office', component:Front_office_profile
    },
    {
      path: '/Front_office_password', name: 'Front_office_password', component:Front_office_password
    },
    {
      path: '/Front_office_view_expenses', name: 'Front_office_view_expenses', component:Front_office_view_expenses
    },
    {
      path: '/Front_office_add_expenses', name: 'Front_office_add_expenses', component:Front_office_add_expenses
    },
    {
      path: '/Front_office_view_booking', name: 'Front_office_view_booking', component:Front_office_view_booking
    },
    // end of front office

    // Supervisor
    {
      path: '/Supervisor_view_booking', name: 'Supervisor_view_booking', component:Supervisor_view_booking
    },
    {
      path: '/Supervisor_profile', name: 'Supervisor_profile', component:Supervisor_profile
    },
    {
      path: '/Supervisor_add_booking', name: 'Supervisor_add_booking', component:Supervisor_add_booking
    },
    // end of Supervisor
   
    // Font_desk
    {
      path: '/Front_Desk',  name: 'Front_Desk', component:Front_Desk
    },
    {
      path: '/Front_Desk_expense_add',  name: 'Front_Desk_expense_add', component:Front_Desk_expense_add
    },
    {
      path: '/Front_Desk_expense_view',  name: 'Front_Desk_expense_view', component:Front_Desk_expense_view
    },
    {
      path: '/Front_desk_password',  name: 'Front_desk_password', component:Front_desk_password
    },
    {
      path: '/Front_Desk_profile',  name: 'Front_Desk_profile', component:Front_Desk_profile
    },
    {
      path: '/Front_desk_view_booking', name: 'Front_desk_view_booking', component:Front_desk_view_booking
    },
    {
      path: '/Front_desk_add_booking', name: 'Front_desk_add_booking',  component:Front_desk_add_booking
    },
    // end of Front_desk
   
    // Room_division_manager
    {
      path: '/Room_division_manager_add_room', name: 'Room_division_manager_add_room',  component:Room_division_manager_add_room
    },
    {
      path: '/Room_division_manager_profile', name: 'Room_division_manager_profile',  component:Room_division_manager_profile
    },
    {
      path: '/Room_division_manager_view_room', name: 'Room_division_manager_view_room',  component:Room_division_manager_view_room
    },
    {
      path: '/Room_division_manager_view_expenses', name: 'Room_division_manager_view_expenses',  component:Room_division_manager_view_expenses
    },
    {
      path: '/Room_division_manager_add_expenses', name: 'Room_division_manager_add_expenses',  component:Room_division_manager_add_expenses
    },
    // end of Room_division_manager

    // F&B Manager
    {
      path: '/FB_manager_create_order', name: 'FB_manager_create_order',  component:FB_manager_create_order
    },
    {
      path: '/FB_Manager_profile', name: 'FB_Manager_profile',  component:FB_Manager_profile
    },
    {
      path: '/FB_manager_view_menu', name: 'FB_manager_view_menu',  component:FB_manager_view_menu
    },
    {
      path: '/FB_manager_create_menu', name: 'FB_manager_create_menu',  component:FB_manager_create_menu
    },
    {
      path: '/FB_manager_view_order', name: 'FB_manager_view_order',  component:FB_manager_view_order
    },
    // end of F&B Manager


    // F&B Supervisor
    {
      path: '/FB_supervisor_view_menu', name: 'FB_supervisor_view_menu',  component:FB_supervisor_view_menu
    },
    {
      path: '/FB_supervisor_profile', name: 'FB_supervisor_profile',  component:FB_supervisor_profile
    },
    {
      path: '/FB_supervisor_create_menu', name: 'FB_supervisor_create_menu',  component:FB_supervisor_create_menu
    },
    {
      path: '/FB_supervisor_view_order', name: 'FB_supervisor_view_order',  component:FB_supervisor_view_order
    },
    {
      path: '/FB_supervisor_create_order', name: 'FB_supervisor_create_order',  component:FB_supervisor_create_order
    },
    {
      path: '/FB_supervisor_password', name: 'FB_supervisor_password',  component:FB_supervisor_password
    },
    // F&B Supervisor

    // Cashiers
    {
      path: '/Cashier_view_menu', name: 'Cashier_view_menu',  component:Cashier_view_menu
    },
    {
      path: '/Cashier_password', name: 'Cashier_password',  component:Cashier_password
    },
    {
      path: '/Cashiers_profile', name: 'Cashiers_profile',  component:Cashiers_profile
    },
    {
      path: '/Cashier_create_menu', name: 'Cashier_create_menu',  component:Cashier_create_menu
    },
    {
      path: '/Cashier_view_order', name: 'Cashier_view_order',  component:Cashier_view_order
    },
    {
      path: '/Cashier_create_order', name: 'Cashier_create_order',  component:Cashier_create_order
    },
    // end of Cashiers

    // Waiter or waitress
    {
      path: '/Waiter_create_menu', name: 'Waiter_create_menu',  component:Waiter_create_menu
    },
    {
      path: '/Waiter_profile', name: 'Waiter_profile',  component:Waiter_profile
    },
    {
      path: '/Waiter_view_menu', name: 'Waiter_view_menu',  component:Waiter_view_menu
    },
    {
      path: '/Waiter_view_order', name: 'Waiter_view_order',  component:Waiter_view_order
    },
    {
      path: '/Waiter_create_order', name: 'Waiter_create_order',  component:Waiter_create_order
    }
// end of Waiter or waitress 
  ]
})
// router.beforeEach((to, from, next) => {
//   if(to.matched.some(record => record.meta.requiresAuth)) {
//     if (store.getters.isLoggedIn) {
//       next()
//       return
//     }
//     next('/login') 
//   } else {
//     next() 
//   }
// })

// export default router