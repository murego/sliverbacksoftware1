import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields';
// import axios from './axios-auth';
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'https://sliverback.hotelsconnect.rw/public/api'

export default new Vuex.Store({
  state: {
		token: localStorage.getItem('token') || null,
		level: localStorage.getItem('level') || null,
		// HITEL OWNER START HERE
		totalsale1: localStorage.getItem('totalsale1') || null,
		ownerhotel_id: localStorage.getItem('ownerhotel_id') || null,
		firstname: localStorage.getItem('firstname') || null,
		email1: localStorage.getItem('email1') || null,
		phonenumber: localStorage.getItem('phonenumber') || null,
		identity: localStorage.getItem('identity') || null,
		num_allowed1: localStorage.getItem('num_allowed1') || null,
		Hotels:[],
		Message:'',
		// END OF OWNER HOTEL

		// GENERAL MANAGER START HERE
		hotel_id: localStorage.getItem('hotel_id') || null,
		Expense:[],
		totalexpense: localStorage.getItem('totalexpense') || null,
		// GENERAL MANAGER END HERE


	     // ROOM DIVISION MANAGER START HERE
	    roomtotalexpenses: localStorage.getItem('roomtotalexpense') || null,
		roomallexpense:[],
		profile:[],
		allmenuitems:[],
		allorders:[],
		// totalexpense: localStorage.getItem('totalexpense') || null,
		// ROOM DIVISION MANAGER END HERE

	// START OF FRONT OFFICE

	// END OF FRONT OFFICE

	// START OF FONT DESK OPERATOR
	// END OF FONT DESK OPERATOR

	// START OF SUPERVISOR
	// END OF SUPERVISOR

	// START OF FB MANAGER
	// END OF FB MANAGER

	// START OF FB SUPERVISOR
	// END OF FB SUPERVISOR

	// START OF CASHIER
	// END OF CASHIER
	
	// START OF WAITER
	// END OF WAITER
  },
  getters: {
	getField,
	level(state) {
		return state.level
	  },
	  loggedIn(state) {
		return state.token !== null
	  },
	  // HITEL OWNER START HERE
	  totalsale1(state) {
		return state.totalsale1
	  },
	  ownerhotel_id(state) {
		return state.ownerhotel_id
	  },
	  f_name(state) {
		return state.firstname
	  },
	  email(state) {
		return state.email1
	  },
	  phone(state) {
		return state.phonenumber
	  },
	  Identity(state) {
		return state.identity
	  },
	  num_allowed(state) {
		return state.num_allowed1
	  },
    retrieveTodos(state){
		return state.Hotels
	},
	changepassword(state){
		return state.Message
	},
	// END OF OWNER HOTEL

	// GENERAL MANAGER START HERE
	hotel_id(state){
		return state.hotel_id
	},
	getExpense(state){
		return state.Expense
	},
	totalexpense(state){
		return state.totalexpense
	},
	// END OF GENERAL MANAGER

	// ROOM DIVISION MANAGER START HERE
	roomExpense(state){
		return state.roomallexpense
	},
	allmenu(state){
		return state.allmenuitems
	},
	allorder(state){
		return state.allorders
	},
	roomtotalexpensess(state){
		return state.roomtotalexpenses
	},
	showprofile(state){
		return state.profile
	},
	managershowprofile(state){
		return state.profile
	},
	// END OF ROOM MANAGER 	
	
		// START OF FRONT OFFICE

	// END OF FRONT OFFICE

	// START OF FONT DESK OPERATOR
	// END OF FONT DESK OPERATOR

	// START OF SUPERVISOR
	// END OF SUPERVISOR

	// START OF FB MANAGER
	// END OF FB MANAGER

	// START OF FB SUPERVISOR
	// END OF FB SUPERVISOR

	// START OF CASHIER
	// END OF CASHIER
	
	// START OF WAITER
	// END OF WAITER
},

  mutations: {
	updateField,
    login(state, level) {
		state.level = level
	  },
	  login(state, token) {
		state.token = token
	  },
	  // HITEL OWNER START HERE
	  Allsalesprice(state, totalsale1) {
		state.totalsale1 = totalsale1
	  },
	  retrieveTodos(state, ownerhotel_id) {
		state.ownerhotel_id = ownerhotel_id
	  },
	  retrieveTodos(state, firstname) {
		state.firstname = firstname
	  },
	  retrieveTodos(state, email1) {
		state.email1 = email1
	  },
	  retrieveTodos(state, phonenumber) {
		state.phonenumber = phonenumber
	  },
	  retrieveTodos(state, identity) {
		state.identity = identity
	  },
	  retrieveTodos(state, num_allowed1) {
		state.num_allowed1 = num_allowed1
	  },
	  retrieveTodos(state, Hotels) {
		state.Hotels = Hotels
	  },
	  changepassword(state, Message) {
		state.Message = Message
	  },
	  // END OF OWNER HOTEL

	//   GENERAL MANAGER START HERE
	viewGenManager1(state, hotel_id) {
		state.hotel_id = hotel_id
	  },
	  getExpense(state, Expense) {
		state.Expense = Expense
	  },
	  viewallexpense(state, totalexpense) {
		state.totalexpense = totalexpense
	  },
	//   END OF GENERAL MANAGER

	// ROOM DIVISION MANAGER START HERE
	roomExpense(state, roomallexpense) {
		state.roomallexpense = roomallexpense
	  },
	  allmenu(state, allmenuitems) {
		state.allmenuitems = allmenuitems
	  },
	  allorder(state, allorders) {
		state.allorders = allorders
	  },
	  roomtotalexpensess(state, roomtotalexpenses) {
		state.roomtotalexpenses = roomtotalexpenses
	  },
	  showprofile(state, profile) {
		state.profile = profile
	  },
	  managershowprofile(state, profile) {
		state.profile = profile
	  },
	// ROOM DEVISION MANAGER ENDS HERE

		// START OF FRONT OFFICE

	// END OF FRONT OFFICE

	// START OF FONT DESK OPERATOR
	// END OF FONT DESK OPERATOR

	// START OF SUPERVISOR
	// END OF SUPERVISOR

	// START OF FB MANAGER
	// END OF FB MANAGER

	// START OF FB SUPERVISOR
	// END OF FB SUPERVISOR

	// START OF CASHIER
	// END OF CASHIER
	
	// START OF WAITER
	// END OF WAITER
  },
  actions: {
	//   for admin panel
	login (context, authData){
		// return new Promise((resolve, reject) =>{
			axios.post('/login',{
				login: authData.login,
				password: authData.password
			})
			.then(res => {
				console.log(res)
			const data = res.data
			 console.log(data)
			for(let todo in data){
			const level = data[todo].level
			console.log(level)
			// if(level==1){

			// }
			const token = data[todo].token
			console.log(token)
			localStorage.setItem('token', token)
			localStorage.setItem('level', level)
			axios.defaults.headers.common['Authorization'] = token
			context.commit('login', token)
			context.commit('login', level)
			// resolve(response)
			}
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	AddPartner (context, authData){
        axios.post('/partner',{
			f_name:authData.f_name,
			l_name:authData.l_name,
			email:authData.email,
			username:authData.username,
			phone:authData.phone, 
			identity:authData.identity,
			password:authData.login, 
			confirmPassword:authData.confirmPassword,
			num_allowed:authData.num_allowed,
			level:authData.level,
        })
        .then(res => {
			console.log(res)
		const data = res.data
		 console.log(data)
							})
        .catch(error => console.log(error))
	},
	AdminViewAllHotels (context, authData){
        axios.post('/partner',{
			f_name:authData.f_name,
			l_name:authData.l_name,
			email:authData.email,
			username:authData.username,
			phone:authData.phone, 
			identity:authData.identity,
			password:authData.login, 
			confirmPassword:authData.confirmPassword,
			num_allowed:authData.num_allowed,
			level:authData.level,
        })
        .then(res => {
			console.log(res)
		const data = res.data
		 console.log(data)
							})
        .catch(error => console.log(error))
	},
	// end of   for admin panel

	// START OF HOTEL OWNER
    retrieveTodos(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
  
        axios.post('/viewInfo')
          .then(response => {
			// console.log(response)
			// console.log(response.data)
			let data11=response.data
			// console.log(data11.Data)
			let data1=data11.Data
			console.log(data1)
			let ownerhotel_id=data1.hotel_id
			console.log(ownerhotel_id)
			let firstname=data1.f_name
			let email1=data1.email
			let phonenumber=data1.phone
			let identity=data1.ID
			let num_allowed1=data1.num_allowed
			localStorage.setItem('ownerhotel_id', ownerhotel_id)
			localStorage.setItem('firstname', firstname)
			localStorage.setItem('email1', email1)
			localStorage.setItem('phonenumber', phonenumber)
			localStorage.setItem('identity', identity)
			localStorage.setItem('num_allowed1', num_allowed1)
			// console.log(data1.Hotels)
			context.commit('retrieveTodos', ownerhotel_id)
			context.commit('retrieveTodos', firstname)
			context.commit('retrieveTodos', email1)
			context.commit('retrieveTodos', phonenumber)
			context.commit('retrieveTodos', identity)
			context.commit('retrieveTodos', num_allowed1)

            context.commit('retrieveTodos', data1.Hotels)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  changepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword1',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	Allsalesprice (context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
	   let ownerhotel_id= context.state.ownerhotel_id
	//    alert(ownerhotel_id)
		axios.post('/allSales', {ownerhotel_id})
		// ownerhotel_id
          .then(response => {
			// console.log(response.data)
			let totalsales=response.data
			// console.log(totalsales.Data)
			let totalsale=totalsales.Data
			console.log(totalsale)
			for(let todo in totalsale){
				const totalsale1 = totalsale[todo].Total_amount
				console.log(totalsale1)
				localStorage.setItem('totalsale1', totalsale1)
				context.commit('Allsalesprice', totalsale1)
				// resolve(response)
				}
            // context.commit('retrieveTodos', data1.Hotels)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	// END OF HOTEL OWNER

	// STRART OF GENERAL MANAGER= BRANCHER MANAGER ON MY WEBSITE
	viewGenManager1 (context){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/viewGenManager')
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Data)
			 let generalmanager=data.Data
				for(let todo in generalmanager){
					const hotel_id = generalmanager[todo].hotel_id
					console.log(hotel_id + 'donat')
					localStorage.setItem('hotel_id', hotel_id)
					context.commit('viewGenManager1', hotel_id)					
				}
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	addexpense (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/addexpenses',{
				hotel_id,
				type: authData.type,
				description: authData.description,
				amount: authData.amount
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	viewallexpense(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/allexpense', {hotel_id})
          .then(response => {
			// console.log(response)
			// console.log(response.data)
			let data11=response.data
			// console.log(data11.Data)
			let data1=data11.Data
			// console.log(data1.Expense)
			let Expense= data1.Expense
			let totalexpense=data1.Total
			// console.log(totalexpense)
			// let ownerhotel_id=data1.hotel_id
			localStorage.setItem('totalexpense', totalexpense)
			// // console.log(data1.Hotels)
			context.commit('viewallexpense', totalexpense)
			context.commit('getExpense', data1.Expense)
          })
          .catch(error => {
            console.log(error)
          })
	  },

	  managershowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/viewGenManager')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('managershowprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },

	// END OF GENERAL MANAGER

	// START OF ROOM DIVISION MANAGER
    roomdivisionshowExpense(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/showExpense', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1.expenses)
			console.log(data1.total)
			context.commit('roomtotalexpensess', data1.total)

            context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  roommanageraddexpense (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/addExpense',{
				hotel_id,
				type: authData.type,
				description: authData.description,
				amount: authData.amount
			})
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	roomchangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword3',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	roomdivisionshowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/profile')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	// END OF ROOM DIVISION MANAGER

	// START OF FRONT OFFICE
	frontofficeshowExpense(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/ownExpense')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1.expenses)
			console.log(data1.total)
			context.commit('roomtotalexpensess', data1.total)

            context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	frontofficeshowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/myprofile')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  frontofficeaddexpense (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/addexpenses2',{
				hotel_id,
				type: authData.type,
				description: authData.description,
				amount: authData.amount
			})
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	frontofficechangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword6',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	// END OF FRONT OFFICE

	// START OF FONT DESK OPERATOR
	frontdeskshowExpense(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/ownExpense3', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1.expenses)
			console.log(data1.total)
			context.commit('roomtotalexpensess', data1.total)

            context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	frontdeskshowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/myprofile2')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  frontdeskaddexpense (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/addexpenses4',{
				hotel_id,
				type: authData.type,
				description: authData.description,
				amount: authData.amount
			})
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	frontdeskchangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword5',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	// END OF FONT DESK OPERATOR

	// START OF SUPERVISOR
	supervisorshowExpense(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/ownExpense7', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1.expenses)
			console.log(data1.total)
			context.commit('roomtotalexpensess', data1.total)

            context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	supervisorshowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/myprofile7')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  supervisoraddexpense (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/addexpenses7',{
				hotel_id,
				type: authData.type,
				description: authData.description,
				amount: authData.amount
			})
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	supervisorchangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword7',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	// END OF SUPERVISOR

	// START OF FB MANAGER
	fbmanagerviewmenu(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/menu', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			// console.log(data1.expenses)
			// console.log(data1.total)
			// context.commit('roomtotalexpensess', data1.total)

            context.commit('allmenu', data11.Data)
          })
          .catch(error => {
            console.log(error)
          })
	  },

	  fbmanagervieworder(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/viewOrder2', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			for(let todo in data1){
				let data2= data1[todo].Orders
				console.log(data2)				
				context.commit('allorder', data1[todo].Orders)
			}
          })
          .catch(error => {
            console.log(error)
          })
	  },
	fbmanagershowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/myprofile11')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  fbmanagercreatemenu (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/createItem',{
				hotel_id,
				meal_name: authData.meal_name,
				categories: authData.categories,
				unit_price: authData.unit_price
			})
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	fbmanagerchangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword9',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	// END OF FB MANAGER

	// START OF FB SUPERVISOR
	fbsupervisorviewmenu(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/menu2', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			// console.log(data1)
			// console.log(data1.total)
			// context.commit('roomtotalexpensess', data1.total)

            context.commit('allmenu', data11.Data)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  fbsupervisorvieworder(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/viewOrder2', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			for(let todo in data1){
				let data2= data1[todo].Orders
				console.log(data2)				
				context.commit('allorder', data1[todo].Orders)
			}
          })
          .catch(error => {
            console.log(error)
          })
	  },
	fbsupervisorshowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/myprofile12')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  fbsupervisorcreatemenu (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/createItem2',{
				hotel_id,
				meal_name: authData.meal_name,
				categories: authData.categories,
				unit_price: authData.unit_price
			})
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	fbsupervisorchangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword10',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	// END OF FB SUPERVISOR

	// START OF CASHIER
	cashierviewmenu(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/menu24', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			// console.log(data1.expenses)
			// console.log(data1.total)
			// context.commit('roomtotalexpensess', data1.total)

            context.commit('allmenu', data11.Data)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	cashiershowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/myprofile24')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  cashiercreatemenu (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/createItem24',{
				hotel_id,
				meal_name: authData.meal_name,
				categories: authData.categories,
				unit_price: authData.unit_price
			})
			.then(res => {
			// console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	cashierchangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword24',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			 console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	// END OF CASHIER
	
	// START OF WAITER
	waiterviewmenu(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id 
        axios.post('/menu25', {hotel_id})
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			console.log(data1)
			// console.log(data1.expenses)
			// console.log(data1.total)
			// context.commit('roomtotalexpensess', data1.total)

            context.commit('allmenu', data11.Data)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	waitershowprofile(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
        axios.post('/myprofile25')
          .then(response => {
			let data11=response.data
			let data1=data11.Data
			// console.log(data1)
			context.commit('showprofile', data11.Data)

            // context.commit('roomExpense', data1.expenses)
          })
          .catch(error => {
            console.log(error)
          })
	  },
	  waitercreatemenu (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		let hotel_id= context.state.hotel_id
		// console.log(hotel_id)
		// return new Promise((resolve, reject) =>{
			axios.post('/createItem25',{
				hotel_id,
				meal_name: authData.meal_name,
				categories: authData.categories,
				unit_price: authData.unit_price
			})
			.then(res => {
			console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	waiterchangepassword (context, authData){
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
		// return new Promise((resolve, reject) =>{
			axios.post('/changePassword25',{
				current_password: authData.current_password,
				new_password: authData.new_password,
				confirmPassword: authData.confirmPassword
			})
			.then(res => {
			console.log(res)
			const data = res.data
			//  console.log(data.Message)
			 let Message=data.Message
			context.commit('changepassword', Message)
			// context.commit('login', level)
			// resolve(response)
			})
			// .catch(error => {
			// 	console.log(error)
			// 	reject(error)
			//   })
			.catch(error => console.log(error))
		// })
	},
	// END OF WAITER
  }
})

